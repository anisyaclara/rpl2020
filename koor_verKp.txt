<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Verifikasi KP</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

</head>
<body>
<!-- Image and text -->
<div class="container-left">
<nav class="navbar navbar-dark bg-dark">
  <a class="navbar-brand" href="/koor">
    <h1>KPSI UKDW</h1>
  </a>
</nav>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="/koor">Dashboard <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/koor/daftar">Daftar Registrasi</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/koor/setBatas">Set Batas KP</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/koor/verSurat">Verifikasi Surat Keterangan KP</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/koor/verPraKp">Verifikasi Pra KP</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/koor/verKp">Verifikasi KP</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/koor/setJadwal">Set Jadwal Ujian</a>
      </li>
    </ul>
  </div>
</nav>
<div class="container-left ml-3">
    <div class="row">
        <table class="table" border=3>
            <tr>
                <th>ID KP</th>
                <th>NIM</th>
                <th>Nama</th>
                <th>Judul</th>
                <th>Dokumen</th>
                <th>Lembaga</th>
                <th>Pimpinan</th>
                <th>Alamat</th>
                <th>Verifikasi</th>
            </tr>

            @php $no=1; @endphp
            @foreach($kp as $p)
            <tr>
              <th scope="row"><?php echo $no++ ?></th>
              <td><?php echo $p->id_kp?></td>
              <td><?php echo $p->nim ?></td>
              <td></td>
              <td><?php echo $p->dokumen ?></td>
              <td><?php echo $p->lembaga ?></td>
              <td><?php echo $p->pimpinan ?></td>
              <td><?php echo $p->alamat ?></td>
              <td>
              <a href="/koor/verKp/{{$p->id_kp}}/acc" class="btn btn-success btn-sm" name="verif" >TERIMA<a/>
              <a href="/koor/verKp/{{$p->id_kp}}/tolak" class="btn btn-danger btn-sm" name="verif" >TOLAK<a/>
              </td>
            </tr>
            @endforeach
        </table>
    </div>
</div> 
</body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</html>
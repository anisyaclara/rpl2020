-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 17, 2020 at 12:09 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rpl2020`
--

-- --------------------------------------------------------

--
-- Table structure for table `dosen`
--

CREATE TABLE `dosen` (
  `nik_dosen` char(8) NOT NULL,
  `nama` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `dosen`
--

INSERT INTO `dosen` (`nik_dosen`, `nama`, `email`, `password`) VALUES
('111111', 'Lumi', 'lumi@si.ukdw.ac.id', 'jszmksa');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_ujian`
--

CREATE TABLE `jadwal_ujian` (
  `id_jadwal` int(3) NOT NULL,
  `nik_dosen` char(8) NOT NULL,
  `nik_koor` char(8) NOT NULL,
  `nim` char(8) NOT NULL,
  `tgl_ujian` date NOT NULL,
  `jam_ujian` time NOT NULL,
  `ruang_ujian` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `koordinator`
--

CREATE TABLE `koordinator` (
  `nik_koor` char(8) NOT NULL,
  `nama` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `koordinator`
--

INSERT INTO `koordinator` (`nik_koor`, `nama`, `email`) VALUES
('222222', 'Hakel', 'hake@staff.ukdw.ac.id');

-- --------------------------------------------------------

--
-- Table structure for table `kp`
--

CREATE TABLE `kp` (
  `id_kp` int(3) NOT NULL,
  `semester` char(5) NOT NULL,
  `tahun` char(4) NOT NULL,
  `nim` char(8) NOT NULL,
  `judul_kp` varchar(30) NOT NULL,
  `tools` varchar(30) NOT NULL,
  `spesifikasi` varchar(30) NOT NULL,
  `lembaga` varchar(30) NOT NULL,
  `pimpinan` varchar(30) NOT NULL,
  `no_telp` varchar(30) NOT NULL,
  `alamat` varchar(30) NOT NULL,
  `fax` varchar(30) NOT NULL,
  `dokumen` varchar(30) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status_kp` varchar(20) NOT NULL DEFAULT 'BELUM DIVERIFIKASI'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kp`
--

INSERT INTO `kp` (`id_kp`, `semester`, `tahun`, `nim`, `judul_kp`, `tools`, `spesifikasi`, `lembaga`, `pimpinan`, `no_telp`, `alamat`, `fax`, `dokumen`, `tanggal`, `status_kp`) VALUES
(1, '2', '2018', '72180260', 'aku', 'oracle', 'database', 'lkjhg', 'sjhgfzgh', 'kjh', 'ljhgtfr', 'kj', 'C:\\xampp\\tmp\\php1A00.tmp', '2020-05-17 06:48:30', '1');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `nim` char(8) NOT NULL,
  `nama` varchar(40) NOT NULL,
  `semester` varchar(5) NOT NULL,
  `tahun` char(4) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`nim`, `nama`, `semester`, `tahun`, `email`, `password`) VALUES
('72180201', 'Deden', 'Genap', '2018', 'deden.prasetio@si.ukdw.ac.id', 'lil'),
('72180260', 'Anisya', 'Genap', '2018', 'anisya.clara@si.ukdw.ac.id', 'lala');

-- --------------------------------------------------------

--
-- Table structure for table `prakp`
--

CREATE TABLE `prakp` (
  `id_praKp` int(3) NOT NULL,
  `nim` char(8) NOT NULL,
  `semester` char(5) NOT NULL,
  `tahun` char(4) NOT NULL,
  `judul_kp` varchar(40) NOT NULL,
  `tools` varchar(30) NOT NULL,
  `spesifikasi` varchar(30) NOT NULL,
  `lembaga` varchar(30) NOT NULL,
  `pimpinan` varchar(30) NOT NULL,
  `no_telp` char(12) NOT NULL,
  `alamat` varchar(30) NOT NULL,
  `fax` varchar(30) NOT NULL,
  `dokumen` varchar(30) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status_prakp` varchar(20) NOT NULL DEFAULT 'BELUM DIVERIFIKASI'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `prakp`
--

INSERT INTO `prakp` (`id_praKp`, `nim`, `semester`, `tahun`, `judul_kp`, `tools`, `spesifikasi`, `lembaga`, `pimpinan`, `no_telp`, `alamat`, `fax`, `dokumen`, `tanggal`, `status_prakp`) VALUES
(1, '72180260', '2', '2018', 'aku', 'oracle', 'database', 'bca', 'kjh', '98765', 'kjhgf', 'mnbg', 'C:\\xampp\\tmp\\php248D.tmp', '2020-05-17 06:43:23', '1'),
(2, '72180260', '2', '2018', 'aku', 'oracle', 'jashd', 'kladjs', 'kGBA', 'kjh', 'LJHGF', ';LHYT', 'C:\\xampp\\tmp\\php6210.tmp', '2020-05-17 06:43:24', '1');

-- --------------------------------------------------------

--
-- Table structure for table `surat_ket`
--

CREATE TABLE `surat_ket` (
  `id_surat` int(3) NOT NULL,
  `nim` char(8) NOT NULL,
  `tahun` char(4) NOT NULL,
  `semester` char(5) NOT NULL,
  `lembaga` varchar(30) NOT NULL,
  `pimpinan` varchar(30) NOT NULL,
  `no_telp` char(12) NOT NULL,
  `alamat` varchar(30) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `dokumen` varchar(30) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status_ket` varchar(20) NOT NULL DEFAULT 'BELUM DIVERIFIKASI'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `surat_ket`
--

INSERT INTO `surat_ket` (`id_surat`, `nim`, `tahun`, `semester`, `lembaga`, `pimpinan`, `no_telp`, `alamat`, `fax`, `dokumen`, `tanggal`, `status_ket`) VALUES
(1, '72180260', '2018', 'Genap', 'Bca', 'lulu', '09928456', 'ksajhgt', 'jsuehwd', 'jxshd', '2020-05-17 07:21:02', '1'),
(2, '72180201', '2018', '2', 'bri', 'mhg', '98765', 'lkjhgfrdftgyj', 'mnbg', 'C:\\xampp\\tmp\\phpA8C9.tmp', '2020-05-17 06:39:12', '1'),
(3, '72180201', '2018', '2', 'lkjhg', 'kiuy', 'kjhg', 'hyt', 'kjhy', 'C:\\xampp\\tmp\\php53C2.tmp', '2020-05-17 06:39:13', '1'),
(4, '72180201', '1945', '2', 'BANK BRI', 'Pak Kardi', '14045', 'Jl.kardi', '21321', 'wadaw', '2020-05-17 06:39:13', '1'),
(7, '72180201', '2312', '1', 'ww', 'wwd', '43', 'wda', 'awdwa', 'dwa', '2020-05-17 06:58:30', '2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`nik_dosen`);

--
-- Indexes for table `jadwal_ujian`
--
ALTER TABLE `jadwal_ujian`
  ADD PRIMARY KEY (`id_jadwal`),
  ADD KEY `nim` (`nim`),
  ADD KEY `nik_dosen` (`nik_dosen`),
  ADD KEY `nik_koor` (`nik_koor`);

--
-- Indexes for table `koordinator`
--
ALTER TABLE `koordinator`
  ADD PRIMARY KEY (`nik_koor`);

--
-- Indexes for table `kp`
--
ALTER TABLE `kp`
  ADD PRIMARY KEY (`id_kp`),
  ADD KEY `nim` (`nim`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`nim`);

--
-- Indexes for table `prakp`
--
ALTER TABLE `prakp`
  ADD PRIMARY KEY (`id_praKp`),
  ADD KEY `nim` (`nim`);

--
-- Indexes for table `surat_ket`
--
ALTER TABLE `surat_ket`
  ADD PRIMARY KEY (`id_surat`),
  ADD KEY `nim` (`nim`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jadwal_ujian`
--
ALTER TABLE `jadwal_ujian`
  MODIFY `id_jadwal` int(3) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kp`
--
ALTER TABLE `kp`
  MODIFY `id_kp` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `prakp`
--
ALTER TABLE `prakp`
  MODIFY `id_praKp` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `surat_ket`
--
ALTER TABLE `surat_ket`
  MODIFY `id_surat` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `jadwal_ujian`
--
ALTER TABLE `jadwal_ujian`
  ADD CONSTRAINT `jadwal_ujian_ibfk_1` FOREIGN KEY (`nim`) REFERENCES `mahasiswa` (`nim`),
  ADD CONSTRAINT `jadwal_ujian_ibfk_2` FOREIGN KEY (`nik_dosen`) REFERENCES `dosen` (`nik_dosen`),
  ADD CONSTRAINT `jadwal_ujian_ibfk_3` FOREIGN KEY (`nik_koor`) REFERENCES `koordinator` (`nik_koor`);

--
-- Constraints for table `kp`
--
ALTER TABLE `kp`
  ADD CONSTRAINT `kp_ibfk_1` FOREIGN KEY (`nim`) REFERENCES `mahasiswa` (`nim`);

--
-- Constraints for table `prakp`
--
ALTER TABLE `prakp`
  ADD CONSTRAINT `prakp_ibfk_1` FOREIGN KEY (`nim`) REFERENCES `mahasiswa` (`nim`);

--
-- Constraints for table `surat_ket`
--
ALTER TABLE `surat_ket`
  ADD CONSTRAINT `surat_ket_ibfk_1` FOREIGN KEY (`nim`) REFERENCES `mahasiswa` (`nim`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

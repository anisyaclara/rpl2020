CREATE VIEW view_dafregistrasi_kp AS
SELECT m.nim, m.nama, k.judul_kp, k.lembaga, k.status_kp
FROM mahasiswa m, kp k
WHERE m.nim = k.nim;

CREATE VIEW view_dafregistrasi_prakp AS
SELECT m.nim, m.nama, p.judul_kp, p.lembaga, p.status_prakp
FROM mahasiswa m, prakp p
WHERE m.nim = p.nim;


<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class tokoBangunan extends CI_Model{
    public function getBarangs(){
        $arr_barang[] = array(1, "Cat", 6,60000);
        $arr_barang[] = array(2, "Seng",15, 25000);
        $arr_barang[] = array(3, "Paku Payung",10,20000);
        $arr_barang[] = array(4, "Engsel", 20,50000);
        $arr_barang[] = array(5, "Kuas Cat",25,65000);
        $arr_barang[] = array(6, "Gegaji",17,150000);
        $arr_barang[] = array(7, "Besi",8,40000);
        $arr_barang[] = array(8, "Baut",15,25000);
        $arr_barang[] = array(9, "Sekrup",13,15000);
        $arr_barang[] = array(10, "Batako",10,30000);
        return $arr_barang;
    }
}
?>
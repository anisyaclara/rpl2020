<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class barang extends CI_Model{
    public function getBarang(){
        $arr_nama[] = array(1, "Cat", 6, "Rp. ". 17000);
        $arr_nama[] = array(2, "Seng", 15, "Rp. ". 25000);
        $arr_nama[] = array(3, "Paku Payung", 10,"Rp. ". 20000);
        $arr_nama[] = array(4, "Engsel", 20, "Rp. ". 50000);
        $arr_nama[] = array(5, "Kuas Cat ", 25, "Rp. ". 65000);
        $arr_nama[] = array(6, "Gergaji", 17, "Rp. ". 150000);
        $arr_nama[] = array(7, "Besi", 8, "Rp. ". 40000);
        $arr_nama[] = array(8, "baut", 15, "Rp. ". 25000);
        $arr_nama[] = array(9, "Sekrup", 13, "Rp. ". 15000);
        $arr_nama[] = array(10, "Batako", 10, "Rp. ". 30000);

        return $arr_nama;
    }
}
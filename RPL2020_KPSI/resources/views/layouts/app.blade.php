<!DOCTYPE html>
<html>
	<head>
		<title>KPSI UKDW</title>
		<link rel="stylesheet" type="text/css" href="css/login.css">		
	</head>
	<body>
			<div class="login"  >
				<img id="img-n" width="100%" height="20%" style="display: block; margin: auto;" src="img/logo-SI.jpg"/>
				<h2 class="login-header">LOGIN KPSI</h2>
				<form class="login-container" action="/login" method="POST">
				{{csrf_field()}}
					<p> 
						<input type="email" placeholder="Email" name="email" id="email" />
					</p>
					<p>
						<input type="password" placeholder="Password" name="password" id="password"/>
					</p>
					<p>
						Note : Gunakan Email UKDW untuk Login
					</p>
					<p>
						<input type="submit" value="LOGIN" placeholder="Login" />
					</p>
				</form>
			</div>
		
	</body>
</html>
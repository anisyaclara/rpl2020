@extends('layouts.mahasiswa')

@section('content')
<div class="container-left">
    <div class="row justify-content-center">
        
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif         
    </div>
</div>
<div class="container-left ml-3">
    <div class="row">
      <table class="table" border=3>
      <tr>
        <th>NIM</th>
        <th>Id Dosen</th>
        <th>Nik Dosen</th>
        <th>Tanggal Ujian</th>
        <th>Jam Ujian</th>
        <th>Ruang ujian Ujian</th>
      </tr>
      @foreach($jadwalMhs as $j)
            <tr>
        <td><?php echo $j->nim ?></td>
        <td><?php echo $j->id_dosen ?></td>
        <td><?php echo $j->nik_dosen ?></td>
        <td><?php echo $j->tanggal ?></td>
        <td><?php echo $j->jam_ujian?></td>
        <td><?php echo $j->ruang_ujian ?></td>
            </tr>
            @endforeach
            </tbody>
      </table>
      </div>
@endsection

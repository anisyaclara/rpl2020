@extends('layouts.koordinator')

@section('content')
<div class="container">
    <div class="row justify-content-center">
     @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
   
<div class="container-left ml-3">
    <div class="row">
        <table class="table" border=3>
            <tr>
              <th>NO</th>
                <th>NIM</th>
                <th>NAMA</th>
                <th>SEMESTER</th>
                <th>TAHUN</th>
                <th>SET BIMBINGAN</th>
            </tr>

            @php $no=1; @endphp
            @foreach($bim as $b)
            <tr>
              <th scope="row"><?php echo $no++ ?></th>
              <td><?php echo $b->nim?></td>
              <td><?php echo $b->nama ?></td>
              <td><?php echo $b->semester ?></td>
              <td><?php echo $b->tahun ?></td>
              <td>           
              <a href="/koor/setBimbingan/{{$b->nim}}" class="btn btn-info btn-sm" name="verif" id="btn" onclick="disable">PILIH DOSEN<a/>
              
              </td>
            </tr>
            @endforeach

        </table>
    </div>
</div>                  
@endsection
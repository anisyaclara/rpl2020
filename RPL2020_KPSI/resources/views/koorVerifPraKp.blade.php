@extends('layouts.koordinator')

@section('content')
     @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <div class="container-left ml-3">
    <div class="row">
        <table class="table" border=3>
            <tr>
                <th>NO</th>
                <th>ID Pra KP</th>
                <th>NIM</th>
                <th>Nama</th>
                <th>Dokumen</th>
                <th>Lembaga</th>
                <th>Pimpinan</th>
                <th>Alamat</th>
                <th>Verifikasi</th>
            </tr>

            @php $no=1; @endphp
            @foreach($prakp as $k)
            <tr>
              <th scope="row"><?php echo $no++ ?></th>
              <td><?php echo $k->id_praKp?></td>
              <td><?php echo $k->nim ?></td>
              <td></td>
              <td><?php echo $k->dokumen ?></td>
              <td><?php echo $k->lembaga ?></td>
              <td><?php echo $k->pimpinan ?></td>
              <td><?php echo $k->alamat ?></td>
              <td>
              <a href="/koor/verPraKp/{{$k->id_praKp}}/acc" class="btn btn-success btn-sm" name="verif" >TERIMA<a/>
              <a href="/koor/verPraKp/{{$k->id_praKp}}/tolak" class="btn btn-danger btn-sm" name="verif" >TOLAK<a/>
              </td>
            </tr>
            @endforeach

        </table>
    </div>
</div>                
@endsection
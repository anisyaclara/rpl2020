@extends('layouts.koordinator')

@section('content')
     @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    
@foreach($b as $b)
<div class="container">
<form method='post' action='/koor/bimbingan/set'>
@php echo csrf_field() @endphp
  <div class="form-group">
    <label for="exampleInputEmail1">NIM</label>
    <input type="email" class="form-control" id="exampleInputEmail1" name="nim" value="@php echo $b->nim @endphp" aria-describedby="emailHelp" readonly>
  </div>
  <div class="form-group">
  <label for="exampleInputEmail1">Pilih Dosen Pembimbing</label>
  <select class="custom-select" name="nik_dosen">
                        <option selected>--Pilih Dosen--</option>
                        <option value="14045">Pak Dosen</option>
                        <option value="14046">Pak Bapak</option>
                    </select>
  </div>
  <button type="submit" class="btn btn-info">SIMPAN</button>
</form>
</div>
@endforeach            
@endsection
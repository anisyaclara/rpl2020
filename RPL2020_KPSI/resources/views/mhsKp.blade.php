@extends('layouts.mahasiswa')

@section('content')
<div class="container-left">
    <div class="row justify-content-center">
        
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif         
    </div>

</div>

<div class="mx-5 mt-5">
<div class="row">
    <!-- left column -->
    
    <div class="col-md-6">
      <!-- general form elements -->
      <div class="box box-dark">
        <div class="box-header with-border">
          <h3 class="box-title">Data Pengajuan KP</h3>
        </div>
        <!-- form start -->
        <table border="1">
        <form role="form" method="post" enctype="multipart/form-data" action="/simpan/kp">
        @php echo csrf_field() @endphp
          <div class="box-body">
            <div class="form-group">
                <label for="semester">Semester:</label>
                <select style="width: 25%" class="form-control" name="semester" id="semester">
                  <option value="Gasal">Gasal</option>
                  <option value="Genap">Genap</option>
                </select>
            </div>
            <div class="form-group">
                <label for="title">Tahun:</label>
                <input type="number" style="width: 25%" class="form-control" name="tahun"/>
            </div>
            <div class="form-group">
                <label for="title">NIM:</label>
                <input type="text" style="width: 25%" class="form-control" name="nim"/>
            </div>
            <div class="form-group">
                <label for="title">Judul:</label>
                <input type="text" class="form-control" name="judul_kp"/>
            </div>
            <div class="form-group">
                <label for="title">Tools:</label>
                <input type="text" class="form-control" name="tools"/>
            </div>
            <div class="form-group">
                <label for="title">Spesifikasi:</label>
                <input type="text" class="form-control" name="spesifikasi"/>
            </div>
            <div class="form-group">
                <label for="title">Lembaga:</label>
                <input type="text" class="form-control" name="lembaga"/>
            </div>
            <div class="form-group">
                <label for="title">Pimpinan:</label>
                <input type="text" class="form-control" name="pimpinan"/>
            </div>
            <div class="form-group">
                <label for="title">No. Telp:</label>
                <input type="text" style="width: 50%" class="form-control" name="no_telp"/>
            </div>
            <div class="form-group">
                <label for="title">Alamat:</label>
                <textarea class="form-control" name="alamat" rows="3"></textarea>
            </div>
            <div class="form-group">
                <label for="title">Fax:</label>
                <input type="text" style="width: 50%" class="form-control" name="fax"/>
            </div>
            <div class="form-group">
                <label for="title">Dokumen:</label>
                <input type="file" class="form-control" name="dokumen"/>
            </div>
          </div>
          <div class="box-footer">
            <button type="submit" class="btn btn-dark">Kirim</button>
          </div>
        </form>
        </table>
        
      </div>
      <!-- /.box -->
    </div>
    
    <!--/.col (left) -->
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Daftar Pengajuan KP</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
          <table class="table table-striped">
            <tbody>
            <tr>
              <th style="width: 10px">No</th>
              <th>Judul</th>
              <th>Pembimbing</th>
              <th>Tanggal</th>
              <th style="width: 40px">Disetujui</th>
            </tr>
            @php $no=1; @endphp
            @foreach($kp as $k)
            <tr>
              <th scope="row"><?php echo $no++ ?></th>
              <td><?php echo $k->judul_kp?></td>
              <td>;lkg</td>
              <td><?php echo $k->tanggal ?></td>
              <td><?php echo $k->status_kp ?></td>
            </tr>
            @endforeach
            </tbody>
            </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
</div>
@endsection

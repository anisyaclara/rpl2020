@extends('layouts.dosen')

@section('content')
@if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
@endif   

<div class="container-left ml-3">
    <div class="row">
      <table class="table" border=3>
      <tr>
        <th>No</th>
        <th>Nim</th>
        <th>Nik Dosen</th>
        <th>Tanggal</th>
        <th>Jam Ujian</th>
        <th>Ruang Ujian</th>
      </tr>
      @php $no=1; @endphp
      @foreach($jadwalDosen as $j)
      <tr>
        <th scope="row"><?php echo $no++ ?></th>
        <td><?php echo $j->nim ?></td>
        <td><?php echo $j->nik_dosen ?></td>
        <td><?php echo $j->tanggal?></td>
        <td><?php echo $j->jam_ujian ?></td>
        <td><?php echo $j->ruang_ujian ?></td>
        </tr>
            @endforeach
      </table>
      </div>
      </div>
    </div>
</div>
@endsection

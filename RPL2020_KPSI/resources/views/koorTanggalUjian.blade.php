@extends('layouts.koordinator')

@section('content')
     @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    
<div class="container-left ml-3">
    <div class="row">
        <table class="table" border=3>
            <tr>
              <th>NO</th>
                <th>NIM</th>
                <th>JUDUL KP</th>
                <th>SEMESTER</th>
                <th>TAHUN</th>
                <th>SET JADWAL</th>
            </tr>

            @php $no=1; @endphp
            @foreach($jadwal as $j)
            <tr>
              <th scope="row"><?php echo $no++ ?></th>
              <td><?php echo $j->nim?></td>
              <td><?php echo $j->judul_kp ?></td>
              <td><?php echo $j->semester ?></td>
              <td><?php echo $j->tahun ?></td>
              <td>           
              <a href="/koor/setJadwal/{{$j->nim}}" class="btn btn-info btn-sm" name="jadwal" id="btn" onclick="disable">SET JADWAL<a/>
              
              </td>
            </tr>
            @endforeach

        </table>
    </div>
</div>             
@endsection
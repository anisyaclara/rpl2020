@extends('layouts.koordinator')

@section('content')
     @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <div class="container-left ml-3">
    <h3>Pra KP </h3>
    <div class="row">
        <table class="table" border=3>
            <tr>
                <th>NO</th>
                <th>NIM</th>
                <th>Nama</th>
                <th>Judul</th>
                <th>Lembaga</th>
            </tr>
            @php $no=1; @endphp
            @foreach($daf as $d)
            <tr>
              <th scope="row"><?php echo $no++ ?></th>
              <td><?php echo $d->nim ?></td>
              <td><?php echo $d->nama ?></td>
              <td><?php echo $d->judul_kp ?></td>
              <td><?php echo $d->lembaga ?></td>
            </tr>
            @endforeach
        </table>
    </div>

    <h3>KP </h3>
    <div class="row">
        <table class="table" border=3>
            <tr>
                <th>NO</th>
                <th>NIM</th>
                <th>Nama</th>
                <th>Judul</th>
                <th>Lembaga</th>
            </tr>
            @php $no=1; @endphp
            @foreach($daf_kp as $dk)
            <tr>
              <th scope="row"><?php echo $no++ ?></th>
              <td><?php echo $dk->nim ?></td>
              <td><?php echo $dk->nama?></td>
              <td><?php echo $dk->judul_kp ?></td>
              <td><?php echo $dk->lembaga ?></td>
            </tr>
            @endforeach
        </table>
    </div>              
@endsection
@extends('layouts.koordinator')

@section('content')
     @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    
@foreach($jadwal as $j)
<div class="container">
<form method='post' action='/koor/jadwal/set'>
@php echo csrf_field() @endphp
  <div class="form-group">
    <label for="exampleInputEmail1">NIM</label>
    <input type="text" class="form-control" id="exampleInputEmail1" name="nim" value="@php echo $j->nim @endphp" aria-describedby="emailHelp" readonly>
  </div>
  <div class="form-group">
                <label for="exampleFormControlSelect1">ID Dosen</label>
                <select class="form-control" id="prodi" name="id_dosen">
                <option>--DOSEN--</option>
                <option value="1">Argo Wibowo</option>
                <option value="2">Erick Kurniawan, S.Kom.,M.Kom.</option>
                <option value="3">Gloria Virginia, S.Kom.,MAI, Ph.D</option>
                <option value="4">Rosa Delima, S.Kom, M.Kom</option>
                <option value="5">Willy Sudiarto Raharjo, S.Kom., M.Cs</option>
                <option value="6">Jong Jek Siang, Drs, M.Sc</option>
                <option value="7">Wimmie Handiwidjojo, Drs., MIT.</option>
                <option value="8">Yetli Oslan, S.Kom, M.T.</option>
                <option value="9">Budi Sutedjo DO, SKom, MM.</option>
                </select>
            </div>
  <div class="form-group">
                <label for="exampleFormControlSelect1">NIK Dosen</label>
                <select class="form-control" id="prodi" name="nik_dosen">
                <option>--NIK DOSEN--</option>
                <option value="23e45243">23e45243</option>
                <option value="074E324">074E324</option>
                <option value="084E325">084E325</option>
                <option value="094E339">094E339</option>
                <option value="104E341">104E341</option>
                <option value="104E344">104E344</option>
                <option value="894E090">894E090</option>
                <option value="944E219">944E219</option>
                <option value="944E217">944E217</option>
                </select>
            </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Tanggal</label>
    <input type="date" class="form-control" name="tanggal">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Jam Ujian</label>
    <input type="text" class="form-control" name="jam_ujian">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Ruang Ujian</label>
    <input type="text" class="form-control" name="ruang_ujian">
  </div>
  <button type="submit" class="btn btn-info">SIMPAN</button>
</form>
</div>
@endforeach              
@endsection
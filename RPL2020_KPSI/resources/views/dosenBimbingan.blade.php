@extends('layouts.dosen')

@section('content')
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif  
    <div class="row">
      <table class="table" border=3>
      <tr>
        <th>NO</th>
        <th>NIM</th>
        <th>Tanggal</th>
      </tr>
      @php $no=1; @endphp
      @foreach($bimbinganDosen as $b)
      <tr>
        <th scope="row"><?php echo $no++ ?></th>
        <td><?php echo $b->nim ?></td>
        <td><?php echo $b->tanggal?></td>
        </tr>
            @endforeach
      </table>
      </div>
      </div> 
    </div>
</div>
@endsection



@extends('layouts.koordinator')

@section('content')
     @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <div class="container-left ml-3">
    <div class="row">
        <table class="table" border=3>
            <tr>
                <th>ID KP</th>
                <th>NIM</th>
                <th>Nama</th>
                <th>Judul</th>
                <th>Dokumen</th>
                <th>Lembaga</th>
                <th>Pimpinan</th>
                <th>Alamat</th>
                <th>Verifikasi</th>
            </tr>

            @php $no=1; @endphp
            @foreach($kp as $p)
            <tr>
              <th scope="row"><?php echo $no++ ?></th>
              <td><?php echo $p->id_kp?></td>
              <td><?php echo $p->nim ?></td>
              <td></td>
              <td><?php echo $p->dokumen ?></td>
              <td><?php echo $p->lembaga ?></td>
              <td><?php echo $p->pimpinan ?></td>
              <td><?php echo $p->alamat ?></td>
              <td>
              <a href="/koor/verKp/{{$p->id_kp}}/acc" class="btn btn-success btn-sm" name="verif" >TERIMA<a/>
              <a href="/koor/verKp/{{$p->id_kp}}/tolak" class="btn btn-danger btn-sm" name="verif" >TOLAK<a/>
              </td>
            </tr>
            @endforeach
        </table>
    </div>
</div>             
@endsection
@extends('layouts.koordinator')

@section('content')
     @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <div class="mx-5 mt-5">
<div class="row">
    <!-- left column -->
    
    <div class="col-md-6">
      <!-- general form elements -->
      <div class="box box-dark">
        <div class="box-header with-border">
          <h3 class="box-title">Set Batas KP</h3>
        </div>
        <!-- form start -->
        <table border="1">
        <form role="form" method="post" enctype="multipart/form-data" action="/simpan/batas">
        @php echo csrf_field() @endphp
          <div class="box-body">
            <div class="form-group">
                <label for="semester">Semester:</label>
                <select style="width: 25%" class="form-control" name="semester" id="semester">
                  <option value="Gasal">Gasal</option>
                  <option value="Genap">Genap</option>
                </select>
            </div>
            <div class="form-group">
                <label for="title">Tanggal Mulai:</label>
                <input type="date" style="width: 50%" class="form-control" name="tgl_mulai"/>
            </div>
            <div class="form-group">
                <label for="title">Tanggal Selesai:</label>
                <input type="date" style="width: 50%" class="form-control" name="tgl_selesai"/>
            </div>
          </div>
          <div class="box-footer">
            <button type="submit" class="btn btn-dark">Kirim</button>
          </div>
        </form>
        </table>
        
      </div>
      <!-- /.box -->
    </div>
    
    <!--/.col (left) -->
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Periode KP</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
          <table class="table table-striped">
            <tbody>
            <tr>
              <th style="width: 10px">No</th>
              <th style="width: 60px">Semester</th>
              <th style="width: 60px">Tanggal Mulai</th>
              <th style="width: 60px">Tanggal Selesai</th>
            </tr>
            @php $no=1; @endphp
            @foreach($batas as $b)
            <tr>
              <th scope="row"><?php echo $no++ ?></th>
              <td><?php echo $b->semester?></td>
              <td><?php echo $b->tgl_mulai ?></td>
              <td><?php echo $b->tgl_selesai ?></td>
            </tr>
            @endforeach
            </tbody>
            </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
</div>        
@endsection
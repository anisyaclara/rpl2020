@extends('layouts.mahasiswa')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="">
                        <div class="row">
                        <div class="col-md-6">
                          <div class="card" style="width: 16rem;">
                            <img src="{{ ('img/dokumenn.png ') }}" class="card-img-top">
                              <div class="card-body">
                              <p class="card-text">Halaman Pengesahan</p>
                              <a href="#" class="btn btn-dark">Belum ada data</a>
                              </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card" style="width: 16rem;">
                              <img src="{{ ('img/dokumenn.png ') }}" class="card-img-top">
                                <div class="card-body">
                                <p class="card-text">Halaman Selesai KP</p>
                                <a href="#" class="btn btn-dark">Belum ada data</a>
                                </div>
                            </div>  
                        </div>
                        </div>
                    </div>      
    </div>
</div>
@endsection

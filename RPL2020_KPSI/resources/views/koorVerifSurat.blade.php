@extends('layouts.koordinator')

@section('content')
     @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <div class="container-left ml-3">
    <div class="row">
        <table class="table" border=3>
            <tr>
              <th>NO</th>
                <th>ID Surat </th>
                <th>NIM</th>
                <th>Dokumen</th>
                <th>Lembaga</th>
                <th>Pimpinan</th>
                <th>Alamat</th>
                <th>Verifikasi</th>
            </tr>

            @php $no=1; @endphp
            @foreach($surat as $s)
            <tr>
              <th scope="row"><?php echo $no++ ?></th>
              <td><?php echo $s->id_surat?></td>
              <td><?php echo $s->nim ?></td>
              <td><?php echo $s->dokumen ?></td>
              <td><?php echo $s->lembaga ?></td>
              <td><?php echo $s->pimpinan ?></td>
              <td><?php echo $s->alamat ?></td>
              <td>           
              <a href="/koor/verSurat/{{$s->id_surat}}/acc" class="btn btn-success btn-sm" name="verif" id="btn" onclick="disable">TERIMA<a/>
              <a href="/koor/verSurat/{{$s->id_surat}}/tolak" class="btn btn-danger btn-sm" name="verif" id="btn" onclick="disabled">TOLAK<a/>
              </td>
            </tr>
            @endforeach

        </table>
    </div>
</div>                
@endsection
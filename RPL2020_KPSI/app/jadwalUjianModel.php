<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jadwalUjianModel extends Model
{
    protected $table = 'jadwal_ujian';
    protected $fillable = ['nim','id_dosen','nik_dosen','tanggal','jam_ujian','ruang_ujian'];
    
    public function dosen(){
        return $this->belongsTo('App\dosenModel','id_dosen');
    }
}

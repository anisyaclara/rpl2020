<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class batasKpModel extends Model
{
    protected $table = 'bataskp';
    protected $fillable = ['semester', 'tgl_mulai','tgl_selesai'];

}

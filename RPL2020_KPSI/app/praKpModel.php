<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class praKpModel extends Model
{
    protected $table = 'prakp';
    protected $fillable =['nim','semester','tahun','judul_kp','tools','spesifikasi','lembaga','pimpinan','no_telp','fax','alamat','dokumen','tanggal','status_prakp'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class suratKpModel extends Model
{
    protected $table = 'surat_ket';
    protected $fillable = ['nim','tahun','semester','lembaga','pimpinan','no_telp','alamat','fax','dokumen','tanggal','status_ket'];
}

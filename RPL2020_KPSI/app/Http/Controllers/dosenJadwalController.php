<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\jadwalUjianModel;

use App\dosenModel;

use Illuminate\Support\Facades\Auth;
class dosenJadwalController extends Controller
{
    
    public function __construct(){
        $this->middleware('cekLogin:dosen');
    }

    public function dosenJadwal()
    {
        $jadwalDosen = jadwalUjianModel::all();
        return view('dosenJadwal',['jadwalDosen'=>$jadwalDosen]);
        
    }
}

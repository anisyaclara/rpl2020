<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\jadwalUjianModel;
use App\kpModel;
class koorJadwalController extends Controller
{
    public function setJadwal()
    {
        return view('koorTanggalUjian');
    }
    public function jadwal()
    {
        $jadwal = kpModel::all();

        return view('/koorTanggalUjian', ['jadwal'=> $jadwal]);
    }
    public function jadwalset($jadwal)
    {
        $jadwal = kpModel::where('nim', $jadwal)->get();

        return view('koorSetTanggalUjian', ['jadwal'=> $jadwal]);
    }
    public function jadwalsave(Request $request)
    {
        jadwalUjianModel::create([
            'nim' => $request->nim,
            'id_dosen' => $request->id_dosen,
            'nik_dosen' => $request->nik_dosen,
            'tanggal' => $request->tanggal,
            'jam_ujian' => $request->jam_ujian,
            'ruang_ujian' => $request->ruang_ujian
        ]);

        return redirect('/koor/setJadwal');
    }
}

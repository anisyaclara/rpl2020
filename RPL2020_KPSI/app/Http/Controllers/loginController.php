<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use App\mahasiswaModel;
use App\koordinatorModel;
use App\dosenModel;
class loginController extends Controller
{
    function login(Request $kiriman){
        $data1=mahasiswaModel::where('email', $kiriman->email)->where('password', $kiriman->password)->get();
        $data2=koordinatorModel::where('email', $kiriman->email)->where('password', $kiriman->password)->get();
        $data3=dosenModel::where('email', $kiriman->email)->where('password', $kiriman->password)->get();

        
        if(count($data1)>0){
            //berhasil login
            Auth::guard('mahasiswa')->check($data1[0]['nim']);
            return redirect('/mhs');
        }elseif(count($data2)>0){
            
            Auth::guard('koordinator')->check($data2[0]['nik_koor']);
            return redirect('/koor');
        }elseif(count($data3)>0){
            
            Auth::guard('dosen')->check($data3[0]['nik_dosen']);
            return redirect('/dosen');
        }
        else{
            return "LOGIN GAGAL";
        }

    }
    function keluar(){

    }
}

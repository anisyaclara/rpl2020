<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\suratKpModel;
use Illuminate\Support\Facades\DB;
class verifSuratController extends Controller
{
    public function verSurat()
    {
        $surat = suratKpModel::where('status_ket', '=', null)->get();
        return view('koorVerifSurat',['surat'=>$surat]);
    }
    public function accSurat(Request $req, $id_surat)
    {
        DB::table('surat_ket')->where('id_surat', $req->id_surat)->update([
            'status_ket' => 1
        ]);

    return redirect('/koor/verSurat');   
    }
    public function tolakSurat(Request $req, $id_surat)
    {
        DB::table('surat_ket')->where('id_surat', $req->id_surat)->update([
            'status_ket' => 2
        ]);

    return redirect('/koor/verSurat');
        
    }
}

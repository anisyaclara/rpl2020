<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\batasKpModel;

class setBatasKpController extends Controller
{
    public function setBatas()
    {
        $batas = batasKpModel::all();
        return view('koorSetBatasKp',['batas'=>$batas]);
    }
    public function simpanSetBatas(Request $request){
        batasKpModel::create([
            'semester'=>$request->semester,
            'tgl_mulai'=>$request->tgl_mulai,
            'tgl_selesai'=>$request->tgl_selesai
        ]);
        return redirect('/koor/setBatas');
    }
}

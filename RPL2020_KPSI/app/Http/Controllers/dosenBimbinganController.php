<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\jadwalUjianModel;
class dosenBimbinganController extends Controller
{
    public function __construct(){
        $this->middleware('cekLogin:dosen');
    }
    
    public function bimbingan()
    {
        $bimbinganDosen = jadwalUjianModel::all();
        return view('dosenBimbingan',['bimbinganDosen'=>$bimbinganDosen]);
    }
}

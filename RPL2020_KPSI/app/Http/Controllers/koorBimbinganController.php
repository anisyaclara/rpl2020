<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\bimbinganModel;
use App\mahasiswaModel;
class koorBimbinganController extends Controller
{
    public function bimbingan()
    {
        $bim = mahasiswaModel::all();

        return view('/koorBimbingan', ['bim'=> $bim]);
    }

    public function bimbinganset($b)
    {
        $b = mahasiswaModel::where('nim', $b)->get();

        return view('koorSetBimbingan', ['b'=> $b]);
    }

    public function bimbingansave(Request $req)
    {
        bimbinganModel::create([
            'nim' => $req->nim,
            'nik_dosen' => $req->nik_dosen 
        ]);

        return redirect('/koor/setBimbingan');
    }
}

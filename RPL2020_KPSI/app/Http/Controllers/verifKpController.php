<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\kpModel;
use Illuminate\Support\Facades\DB;
class verifKpController extends Controller
{
    public function verKp()
    {
        $kp = kpModel::where('status_kp', '=', null)->get();
        return view('koorVerifKp',['kp'=>$kp]);
    }
    public function accKp(Request $req, $id_kp)
    {
        DB::table('kp')->where('id_kp', $req->id_kp)->update([
            'status_kp' => 1
        ]);

    return redirect('/koor/verKp');   
    }
    public function tolakKp(Request $req, $id_kp)
    {
        DB::table('kp')->where('id_kp', $req->id_kp)->update([
            'status_kp' => 2
        ]);
    return redirect('/koor/verKp');   
    }
}

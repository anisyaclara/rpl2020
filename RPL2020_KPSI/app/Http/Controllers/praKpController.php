<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\praKpModel;
class praKpController extends Controller
{
    public function prakp()
    {
        $prakp = praKpModel::all();
        return view('mhsPraKp',['prakp'=>$prakp]);
    }
    public function simpanPrakp(Request $request){
        praKpModel::create([
            'nim' => $request->nim,
            'tahun' => $request->tahun,
            'semester' => $request->semester,
            'judul_kp' => $request->judul_kp,
            'tools' => $request->tools,
            'spesifikasi' => $request->spesifikasi,
            'lembaga' => $request->lembaga,
            'pimpinan' =>$request->pimpinan,
            'no_telp' =>$request->no_telp,
            'alamat' =>$request->alamat,
            'fax' =>$request->fax,
            'dokumen' =>$request->dokumen,
            'tanggal' =>$request->tanggal,
            'status_prakp'=>$request->status_prakp
        ]);
        return redirect('/mhs/prakp');
    }
}

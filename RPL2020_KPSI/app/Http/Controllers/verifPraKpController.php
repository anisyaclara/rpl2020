<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\praKpModel;
use Illuminate\Support\Facades\DB;
class verifPraKpController extends Controller
{
    public function verPraKp()
    {
        $prakp = praKpModel::where('status_prakp', '=', null)->get();
        return view('koorVerifPraKp',['prakp'=>$prakp]);
    }
    public function accPraKp(Request $req, $id_prakp)
    {
        DB::table('prakp')->where('id_praKp', $req->id_prakp)->update([
            'status_prakp' => 1
        ]);

    return redirect('/koor/verPraKp');  
    }
    public function tolakPraKp(Request $req, $id_prakp)
    {
        DB::table('prakp')->where('id_praKp', $req->id_prakp)->update([
            'status_prakp' => 2
        ]);
    return redirect('/koor/verPraKp');   
    }
}

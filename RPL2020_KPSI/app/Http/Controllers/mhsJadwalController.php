<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\jadwalUjianModel;

class mhsJadwalController extends Controller
{
    public function jadwal()
    {
        $jadwalMhs = jadwalUjianModel::all();
        return view('mhsJadwalUjian',['jadwalMhs'=>$jadwalMhs]);
    }
}

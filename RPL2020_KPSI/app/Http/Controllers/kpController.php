<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\kpModel;

class kpController extends Controller
{
    public function kp()
    {
        $kp = kpModel::all();
        return view('mhsKp',['kp'=>$kp]);
    }
    public function simpanKp(Request $request){
        kpModel::create([
            'nim' => $request->nim,
            'tahun' => $request->tahun,
            'semester' => $request->semester,
            'judul_kp' => $request->judul_kp,
            'tools' => $request->tools,
            'spesifikasi' => $request->spesifikasi,
            'lembaga' => $request->lembaga,
            'pimpinan' =>$request->pimpinan,
            'no_telp' =>$request->no_telp,
            'alamat' =>$request->alamat,
            'fax' =>$request->fax,
            'dokumen' =>$request->dokumen,
            'tanggal' =>$request->tanggal,
            'status_kp'=>$request->status_kp
        ]);
        return redirect('/mhs/kp');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\suratKpModel;
class suratController extends Controller
{
    public function suratKeterangan()
    {
        $surat = suratKpModel::all();
        return view('mhsSuratKet',['surat'=>$surat]);
    }
    public function simpanSurat(Request $request){
        suratKpModel::create([
            'nim' => $request->nim,
            'tahun' => $request->tahun,
            'semester' => $request->semester,
            'lembaga' => $request->lembaga,
            'pimpinan' =>$request->pimpinan,
            'no_telp' =>$request->no_telp,
            'alamat' =>$request->alamat,
            'fax' =>$request->fax,
            'dokumen' =>$request->dokumen,
            'tanggal' =>$request->tanggal,
            'status_ket'=>$request->status_ket
        ]);
        return redirect('/mhs/suratketerangan');
    }
}

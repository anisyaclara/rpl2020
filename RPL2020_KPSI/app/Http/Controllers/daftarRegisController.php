<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\daftarRegisKpModel;
use App\daftarRegisPraKpModel;

class daftarRegisController extends Controller
{
    public function daftarRegis()
    {
        $daf = daftarRegisKpModel::where('status_kp','=','1')->get();
        $daf_kp = daftarRegisPraKpModel::where('status_prakp','=','1')->get();

        return view('koorDaftarRegistrasi', ['daf'=> $daf, 'daf_kp'=> $daf_kp]);
    }
}

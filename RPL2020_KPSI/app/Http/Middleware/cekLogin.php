<?php

namespace App\Http\Middleware;
use App\mahasiswaModel;
use App\koordinatorModel;
use App\dosenModel;
use Closure;

class cekLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $data1=mahasiswaModel::where('email', $request->email)->where('password', $request->password)->get();
        $data2=koordinatorModel::where('email', $request->email)->where('password', $request->password)->get();
        $data3=dosenModel::where('email', $request->email)->where('password', $request->password)->get();

        
        if(count($data1)>0){
            //berhasil login
            return redirect('/mhs');
        }elseif(count($data2)>0){
            return redirect('/koor');
        }elseif(count($data3)>0){
            return redirect('/dosen');
        }
        else{
            return $next($request);
        }
    }
}

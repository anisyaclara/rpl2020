<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mahasiswaModel extends Model
{
    protected $table = 'mahasiswa';
    public function suratKp(){
        return $this->hasMany('App\suratKpModel');
    }
    public function praKp(){
        return $this->belongsTo('App\praKpModel');
    }
    public function kp(){
        return $this->belongsTo('App\kpModel');
    }
}

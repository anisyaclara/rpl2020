<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bimbinganModel extends Model
{
    protected $table = 'bimbingan';
    public function dosen(){
        return $this->belongsTo('App\dosenModel','id_dosen');
    }
}

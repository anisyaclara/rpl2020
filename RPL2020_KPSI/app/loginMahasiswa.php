<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class loginMahasiswa extends Authenticatable
{
    protected $table='mahasiswa';
    protected $fillable = [
        'nama', 'email', 'password'
    ];
}

<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class loginDosen extends Authenticatable
{
    use Notifiable;
    protected $tabel = 'dosen';
    protected $fillable = ['id_dosen','name','nik_dosen'];
}

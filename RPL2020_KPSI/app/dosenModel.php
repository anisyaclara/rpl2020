<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dosenModel extends Model
{
    protected $table = 'dosen';
    protected $fillable = ['id_dosen','name','nik_dosen'];
}

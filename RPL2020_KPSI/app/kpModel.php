<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kpModel extends Model
{
    protected $table='kp';
    protected $fillable = ['semester','tahun','nim','judul_kp','tools','spesifikasi','lembaga','pimpinan','no_telp','alamat','fax','dokumen','tanggal','status_kp'];
}

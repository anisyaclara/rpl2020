<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', function () {
    return view('login');
});
//mahasiswa
Route::get('/mhs', 'mahasiswaController@mhs')->middleware('cekLogin');
Route::get('/mahasiswa/{id}','loginController@test')->middleware('cekLogin');
//dosen
Route::get('/dosen', 'dosenController@dosen')->middleware('cekLogin');
//koordinator
Route::get('/koor', 'koordinatorController@koor')->middleware('cekLogin');
//surat keterangan kp
Route::get('/mhs/suratketerangan', 'suratController@suratKeterangan')->middleware('cekLogin');
Route::post('/surat/simpan', 'suratController@simpanSurat')->middleware('cekLogin');


//pra kp
Route::get('/mhs/prakp', 'praKpController@prakp')->middleware('cekLogin');
Route::post('/simpan/prakp', 'praKpController@simpanPrakp')->middleware('cekLogin');

//kp
Route::get('/mhs/kp', 'kpController@kp')->middleware('cekLogin');
Route::post('/simpan/kp', 'kpController@simpanKp')->middleware('cekLogin');

// koordinator set bimbingan
Route::get('/koor/setBimbingan', 'koorBimbinganController@bimbingan')->middleware('cekLogin');
Route::get('/koor/setBimbingan/{nim}', 'koorBimbinganController@bimbinganset')->middleware('cekLogin');
Route::post('/koor/bimbingan/set', 'koorBimbinganController@bimbingansave')->middleware('cekLogin');

// koordinator set jadwal
Route::get('/koor/setJadwal', 'koorJadwalController@jadwal')->middleware('cekLogin');
Route::post('/koor/jadwal/set', 'koorJadwalController@jadwalsave')->middleware('cekLogin');
Route::get('/koor/setJadwal/{nim}', 'koorJadwalController@jadwalset')->middleware('cekLogin');

// koordinator verifikasi surat keterangan
Route::get('/koor/verSurat', 'verifSuratController@verSurat')->middleware('cekLogin');
Route::get('/koor/verSurat/{id_surat}/acc', 'verifSuratController@accSurat')->middleware('cekLogin');
Route::get('/koor/verSurat/{id_surat}/tolak', 'verifSuratController@tolakSurat')->middleware('cekLogin');

// koordinator verifikasi pra kp
Route::get('/koor/verPraKp', 'verifPraKpController@verPraKp')->middleware('cekLogin');
Route::get('/koor/verPraKp/{id_prakp}/acc', 'verifPraKpController@accPraKp')->middleware('cekLogin');
Route::get('/koor/verPraKp/{id_prakp}/tolak', 'verifPraKpController@tolakPraKP')->middleware('cekLogin');

// koordinator verifikasi kp
Route::get('/koor/verKp', 'verifKpController@verKp')->middleware('cekLogin');
Route::get('/koor/verKp/{id_kp}/acc', 'verifKpController@accKp')->middleware('cekLogin');
Route::get('/koor/verKp/{id_kp}/tolak', 'verifKpController@tolakKP')->middleware('cekLogin');

// koordinator lihat daftar registrasi
Route::get('/koor/daftar', 'daftarRegisController@daftarRegis')->middleware('cekLogin');

//login
Route::post('/login', 'loginController@login')->middleware('cekLogin');


//mahasiswa tanggal ujian
Route::get('/mhs/tglujian', 'mhsJadwalController@jadwal')->middleware('cekLogin');

//dosen bimbingan
Route::get('/dosen/bimbingan', 'dosenBimbinganController@bimbingan')->middleware('cekLogin');

//dosen jadwal ujian
Route::get('/dosen/jadwal', 'dosenJadwalController@dosenJadwal')->middleware('cekLogin');

//koordinator set batas kp
Route::get('/koor/setBatas', 'setBatasKpController@setBatas')->middleware('cekLogin');
Route::post('/simpan/batas', 'setBatasKpController@simpanSetBatas')->middleware('cekLogin');







